# Disclaimer
These feeds are included in this repo for testing purposes. Rumraisin has at one
point or another had trouble parsing them, which is why they've been kept as
tests to make sure that no regressions are introduced once the corresponding
issues have been fixed.

I'm no copyright expert, especially not when it comes to syndicated content, so
I have no idea how copyright laws apply to this use case. But I wish to make
it clear that none of the contents in this directory, other than this README
file, was created by me. In an attempt to be fair and not infringe on anyones
rights, I will give credit to the original sources below. Furthermore, I have
not asked for any permissions from the original authors because it is my
understanding that that should not be necessary for content that is by
definition meant to be shared/distributed/syndicated, which is the case for RSS
and Atom feeds.


//

zluudg

## Credit

### daringfireball-net-2024-04-22.feed
Taken from daringfireball.net on Apr 24th, 2024. Original author is John Gruber.

### parabol-press-2024-04-22.feed
Taken from parabol.press on Apr 24th, 2024. Contains texts from various authors,
as seen in the feed metadata. Will not list all of them here. Publisher for the
magazine is Kajsa Ekis Ekman.

### ntia-gov-2024-04-22.feed
Taken from ntia.gov on Apr 24th, 2024. Publisher is NTIA Office of Public
Affairs.
