CURRENT_VER := $(shell sed -nr 's/^version\ =\ \"(([0-9]+\.){2}[0-9]+)\"/\1/p' Cargo.toml | head -1)
DATE := $(shell date +%Y-%m-%d)

bump-major: _bump-major

bump-minor: _bump-minor

bump-patch: _bump-patch

test:
	@cargo test
	@mdbook test docs/rumraisin-book
	@cargo publish --dry-run
	@echo "All tests passed!"

_bump-%: _nondirty test _setup-%
	@sed -i 's/\(^## \[Unreleased\]\)/\1\n\n## [$(NEXT_VER)] - $(DATE)/' CHANGELOG.md
	@sed -i '0,/^version/{s/$(CURRENT_VER)/$(NEXT_VER)/}' Cargo.toml
	@git add -- CHANGELOG.md Cargo.toml
	@git commit -m "RELEASE v$(NEXT_VER)"
	@git tag v$(NEXT_VER)

_nondirty:
	@echo "Checking if there are uncommited changes..."
	@git diff-index --quiet HEAD -- #
	@echo "Checking if there are untracked files..."
	@test -z "$$(git ls-files --others --exclude-standard)"

_setup-major:
	$(eval NEXT_VER := $(shell echo $(CURRENT_VER) | awk -F '.' '{ OFS="."; $$1=$$1+1; $$2=0; $$3=0; print $$0 }'))

_setup-minor:
	$(eval NEXT_VER := $(shell echo $(CURRENT_VER) | awk -F '.' '{ OFS="."; $$2=$$2+1; $$3=0; print $$0 }'))

_setup-patch:
	$(eval NEXT_VER := $(shell echo $(CURRENT_VER) | awk -F '.' '{ OFS="."; $$3=$$3+1; print $$0 }'))
