# Changelog

This document keeps track of notable changes.
  
Based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/)  
Adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

## [Unreleased]

### Added
- Tests for the parser using real world feeds

### Fixed
- Occasional crash due to "connection closed before message completed" in hyper

## [0.2.0] - 2024-04-07

### Added
- Read configuration file location from env var or cli arg (or default location)
- Set workdir location from env var or cli arg (default to current directory)
- Added "-d" cli flag for controlling debug output verbosity
- Added "-f" and "-p" cli flags for setting fetch and publish periods
- Fetch and publish periods can be set in config file
- Slightly neater CTRL+C exit
- Added "generate" subcommand for generating config and registration templates
- Added a usage guide using mdbook
- Added a Makefile for creating releases
- Publish docs using GitLab Pages
- Metadata for crates.io

### Changed
- Go for a more application-like architecture
- Made application async
- No longer depends on "dirs" or "yaml-rust" crates
- Added direct dependencies on "serde", "figment" and "clap" crates 
- Use SeaORM for handling database transaction with storage.rs as a facade
- Now spawns separate tokio tasks for fetch and publishing
- Slightly less minimalistic README

## [0.1.0] - 2024-01-15

### Added
- Read feed URLs from config file
- Create a Matrix room for each feed
- Invite users specified in config file to created rooms
- Fetch feed entries and post title + link in corresponding room
- Repeat every 30 mins forever
- Run with `cargo run`
