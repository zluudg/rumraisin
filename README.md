# Rumraisin
An RSS/Atom appservice for the [Matrix](https://matrix.org) ecosystem.

## Documentation
Installation and configuration instructions available at
https://zluudg.gitlab.io/rumraisin.

## Source
Source code available at https://gitlab.com/zluudg/rumraisin.

## Crates.io
Find the latest release at https://docs.rs/crate/rumraisin/latest.

## Example
![](img/rumraisin-example.png)
Screenshot taken while using the [Cinny](https://cinny.in) client.
