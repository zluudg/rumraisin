# Introduction
Rumraisin is a Matrix application service that allows you to use any Matrix
client as a RSS/Atom aggregator. It lets you follow your feeds from
multiple devices while keeping the read/unread status in sync.

Rumraisin is best suited for single-tenant homeservers. There is currently no
support for letting different users follow different sets of feeds. Nor are 
there any features for adding/deleting feeds dynamically, such as by sending
commands to a bot.

However, if you run your own Matrix homeserver and just want a simple means of
tracking the same set of feeds across multiple devices, Rumraisin could fit
your case.

## Free Software
Rumraisin is free and open-source software. The source-code for Rumraisin (and
for this book), is available at [GitLab][rumraisin] under the [MIT License].

[rumraisin]: https://gitlab.com/zluudg/rumraisin
[MIT License]: https://gitlab.com/zluudg/rumraisin/-/raw/main/LICENSE
