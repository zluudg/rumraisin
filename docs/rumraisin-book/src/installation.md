# Installation
Rumraisin is intended to be run as a systemd service by a dedicated user. To
achieve this do the following:

## 1. Create a system user
```shell
$ sudo adduser --system rumraisin --home /opt/rumraisin
```

## 2. Build the Rumraisin binary:
It is recommended to run the build steps, whichever are chosen, as the
newly created `rumraisin` user. To get a shell as `rumraisin`, run:

```shell
$ sudo -U rumraisin bash
```

Then proceed to build either from source or from crates.io.

### From source
```shell
$ cd ~
$ git clone https://gitlab.com/zluudg/rumraisin.git rumraisin-repo
$ cd rumraisin-repo
$ cargo build --release
```

### From crates.io
```shell
$ cargo install rumraisin
```

## 3. Generate configuration and registration files
Rumraisin can generate template configuration and registration files. Do
this by issuing something like:

```shell
$ rumraisin generate -a https://matrix.mydomain.com -d mydomain.com
```

This will create a file `registration.yaml` that is to be read by the
homeserver and thus must have appropriate read permissions. One way to achive
this is to make whatever user the homeserver runs as the owner of the file:

```shell
$ shudo chown homeserver-user:nogroup registraton.yaml
```

Also, make sure to update your homeserver's configuration file to point to
`registration.yaml`.

The `generate` command will also create a file called `config.yaml`, which
is used to configure Rumraisin. Details about that
[in the next section](configuration.md). Edit the file as you see fit. Most
certainly, you want to update it to contain your username and your desired
feeds. Make sure that it is readable by the `rumraisin` user.

## 4. Create systemd service file
Open `/etc/systemd/system/rumraisin.service` in your favourite text editor and
paste the following:

```ini
[Unit]
Description=Rumraisin matrix-RSS/Atom bridge

[Service]
Type=exec
User=rumraisin
WorkingDirectory=/opt/rumraisin
ExecStart=/<PATH>/<TO>/<RUMRAISIN>
Restart=on-failure
RestartSec=30s
StandardOutput=journal+console
StandardError=journal+console
Environment="RUMRAISIN_WORKDIR=/opt/rumraisin/.rumraisinrc/"
Environment="RUMRAISIN_CONFIG_FILE=/opt/rumraisin/.rumraisinrc/config.yaml"

[Install]
WantedBy=multi-user.target
```

Replace the value for `ExecStart` depending on how the binary was built. If
it was built [from source](#from-source), the value should be
`/opt/rumraisin/rumraisin-repo/target/release/rumraisin`. If it was built
[from crates.io](#from-cratesio), the value should be
`/opt/rumraisin/.cargo/bin`.

Also, make sure that the environment variables have appropriate read/write
permissions and that `RUMRASIN_CONFIG_FILE` points to the config file that
was generated in
[the previous step](#3-generate-configuration-and-registration-files).

## 5. Start and enable the service
```shell
$ systemctl daemon-reload
$ systemctl start rumraisin.service
$ systemctl enable rumraisin.service
```

## 6. Check your client
If everything went as it should, you should now have received a number of
invites from Rumraisin's bot user (typically named `rumraisinbot`) in your
Matrix client. There will be one room per feed and each room will be populated
with the current state of the feed and updated as items get added to the feed.
