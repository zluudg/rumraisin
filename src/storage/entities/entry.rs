use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use sea_orm::entity::prelude::*;

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Eq)]
#[sea_orm(table_name="entry")]
pub struct Model {
    pub title: String,
    pub link: String,
    pub source: String,
    pub kind: String,
    pub is_published: bool,
    pub uid: String,
    #[sea_orm(primary_key, auto_increment=false)]
    pub hash: i64,
}

impl Hash for Model {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.link.hash(state);
        self.uid.hash(state);
    }
}

impl Model {
    pub fn get_hash(&self) -> i64 {
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        s.finish() as i64
    }
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(belongs_to="super::feed::Entity",
              from="Column::Source",
              to="super::feed::Column::Url",
              on_update="Cascade",
              on_delete="Cascade")]
    Feed,
}

impl Related<super::feed::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Feed.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
