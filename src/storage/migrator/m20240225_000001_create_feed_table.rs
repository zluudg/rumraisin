use sea_orm_migration::prelude::*;

pub struct Migration;

impl MigrationName for Migration {
    fn name(&self) -> &str {
        "m20240225_000001_create_feed_table"
    }
}

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Feed::Table)
                    .col(
                        ColumnDef::new(Feed::Name)
                        .string()
                        .not_null()
                    )
                    .col(
                        ColumnDef::new(Feed::Url)
                        .string()
                        .not_null()
                        .primary_key()
                    )
                    .col(
                        ColumnDef::new(Feed::Kind)
                        .string()
                        .not_null()
                    )
                    .col(
                        ColumnDef::new(Feed::Room)
                        .string()
                    )
                    .to_owned()
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Feed::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum Feed {
    Table,
    Name,
    Url,
    Kind,
    Room,
}
