use sea_orm_migration::prelude::*;

use super::m20240225_000001_create_feed_table::Feed;

pub struct Migration;

impl MigrationName for Migration {
    fn name(&self) -> &str {
        "m20220225_000002_create_entry_table"
    }
}

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Entry::Table)
                    .col(
                        ColumnDef::new(Entry::Title)
                        .string()
                        .not_null()
                    )
                    .col(
                        ColumnDef::new(Entry::Link)
                        .string()
                        .not_null()
                    )
                    .col(
                        ColumnDef::new(Entry::Source)
                        .string()
                        .not_null()
                    )
                    .col(
                        ColumnDef::new(Entry::Kind)
                        .string()
                        .not_null()
                    )
                    .col(
                        ColumnDef::new(Entry::IsPublished)
                        .boolean()
                        .not_null()
                    )
                    .col(
                        ColumnDef::new(Entry::Uid)
                        .string()
                        .not_null()
                    )
                    .col(
                        ColumnDef::new(Entry::Hash)
                        .integer()
                        .not_null()
                        .primary_key()
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .name("FK_entry_feed_url")
                            .from(Entry::Table, Entry::Source)
                            .to(Feed::Table, Feed::Url)
                            .on_delete(ForeignKeyAction::Cascade)
                            .on_update(ForeignKeyAction::Cascade)
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Entry::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum Entry {
    Table,
    Title,
    Link,
    Source,
    Kind,
    IsPublished,
    Uid,
    Hash,
}
