use std::path::PathBuf;

use anyhow::{Context, Result};
use sea_orm::{Database, ConnectOptions};
use sea_orm_migration::prelude::*;
use tracing::{instrument, log};

use super::Storage;
use super::migrator::Migrator;

const DB_FILE: &str = "rumraisin.db";

#[derive(Default, Debug)]
pub struct StorageBuilder {
    workdir: String, // TODO make Option<T> and have proper check that it is set
}

impl StorageBuilder {

    pub fn workdir(mut self, workdir: String) -> StorageBuilder {
        self.workdir = workdir;
        self
    }

    #[instrument(level = "debug")]
    pub async fn build(self) -> Result<Storage> {
        let mut path = PathBuf::from(self.workdir);
        path.push(DB_FILE);
        let path = path.to_str().context("Bad db path")?;

        let mut opt = ConnectOptions::new(
            format!("sqlite://{}?mode=rwc", path)
        );
        opt.sqlx_logging(true);
        opt.sqlx_logging_level(log::LevelFilter::Debug);

        let db = Database::connect(opt).await?;
        Migrator::up(&db, None).await?;

        Ok(Storage { db })
    }
}
