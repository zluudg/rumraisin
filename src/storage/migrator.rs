use sea_orm_migration::prelude::*;

mod m20240225_000001_create_feed_table;
mod m20240225_000002_create_entry_table;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20240225_000001_create_feed_table::Migration),
            Box::new(m20240225_000002_create_entry_table::Migration),
        ]
    }
}
