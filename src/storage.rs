mod builder;
mod migrator;
mod entities;

use anyhow::Result;
use sea_orm::{
    ActiveModelTrait,
    ColumnTrait,
    DatabaseConnection,
    EntityTrait,
    ModelTrait,
    QueryFilter,
};
use sea_query::OnConflict;
use tracing::instrument;

use self::builder::StorageBuilder;
use self::entities::prelude::*;
use self::entities::*;

#[derive(Debug, Default, Clone)]
pub struct Storage {
    pub db: DatabaseConnection,
}

pub type FeedModel = feed::Model;
pub type EntryModel = entry::Model;

impl Storage {

    pub fn builder() -> StorageBuilder {
        StorageBuilder::default()
    }

    #[instrument(level = "debug")]
    pub async fn store_feed(&self, feed: FeedModel) -> Result<()> {
        let old = Feed::find_by_id(&feed.url).one(&self.db).await?;
        let mut new: feed::ActiveModel = feed.into();

        match old {
            Some(o) => {
                new.reset(feed::Column::Name);
                new.reset(feed::Column::Kind);
                if o.room.is_none() {
                    new.reset(feed::Column::Room);
                }
                new.update(&self.db).await?
            }
            None => new.insert(&self.db).await?,
        };

        Ok(())
    }

    pub async fn get_feeds(&self) -> Result<Vec<FeedModel>> {
        let res = Feed::find().all(&self.db).await?;
        Ok(res)
    }

    pub async fn get_feed_by_entry(&self, entry: &EntryModel)
        -> Result<Option<FeedModel>> {
        let res = entry.find_related(Feed).one(&self.db).await?;
        Ok(res)
    }

    #[instrument(level = "trace")]
    pub async fn store_entries(
        &self,
        entries: Vec<EntryModel>,
        update_is_published: bool)
        -> Result<()> {
        let models: Vec<entry::ActiveModel> = entries.iter().map(
            |e| (*e).clone().into()
        )
        .collect();

        if update_is_published {
            // TODO check if ths is scalable. If feed has many entries, the
            // generated string will become very long.
            let _ = Entry::insert_many(models)
                .on_conflict(
                    OnConflict::column(entry::Column::Hash)
                        .update_column(entry::Column::IsPublished)
                        .to_owned()
                )
                .do_nothing() // Don't whine if nothing gets inserted
                .exec(&self.db).await?;
        } else {
            // TODO check if ths is scalable. If feed has many entries, the
            // generated string will become very long.
            let _ = Entry::insert_many(models)
                .on_conflict(
                    OnConflict::column(entry::Column::Hash)
                        .do_nothing()
                        .to_owned()
                )
                .do_nothing() // Don't whine if nothing gets inserted
                .exec(&self.db).await?;
        }

        Ok(())
    }

    pub async fn get_unpublished_entries(&self) -> Result<Vec<EntryModel>> {
        let res = Entry::find()
            .filter(entry::Column::IsPublished.eq(false))
            .all(&self.db).await?;
        Ok(res)
    }
}

#[cfg(test)]
mod test {
    use std::fs;

    use super::*;

    const TMP_WORKDIR: &str = "tmp_rumraisin_test_storage";

    async fn setup() -> Storage {
        fs::create_dir(TMP_WORKDIR)
            .expect("Couldn't create workdir for tests");
        let storage = Storage::builder()
            .workdir(TMP_WORKDIR.to_string())
            .build().await;
        storage.expect("Couldn't create database for testing")
    }

    fn teardown() {
        fs::remove_dir_all(TMP_WORKDIR)
            .expect("Coudln't remove workdir for tests");
    }

    #[tokio::test]
    async fn store_feed_test() {
        let stor = setup();

        let feed = FeedModel {
            name: "test".to_string(),
            url: "https://example.com/feed".to_string(),
            kind: "rss".to_string(),
            room: Some("abc".to_string()),
        };

        let res = stor.await.store_feed(feed).await;

        assert!(res.is_ok());

        teardown();
    }
}
