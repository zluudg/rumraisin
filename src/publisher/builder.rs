use anyhow::{Context, Result};
use tracing::instrument;
use ruma::{
    client::http_client::Reqwest,
    user_id::UserId,
};

use crate::storage::Storage;

use super::Publisher;

#[derive(Default, Debug)]
pub struct PublisherBuilder {
    storage: Option<Storage>,
    homeserver: Option<String>,
    as_token: Option<String>,
    users: Vec<String>,
    domain: Option<String>,
    bot_username: Option<String>,
}

impl PublisherBuilder {

    pub fn storage(mut self, storage: Storage) -> PublisherBuilder {
        self.storage = Some(storage);
        self
    }

    pub fn homeserver(mut self, homeserver: String) -> PublisherBuilder {
        self.homeserver = Some(homeserver);
        self
    }

    pub fn as_token(mut self, as_token: String) -> PublisherBuilder {
        self.as_token = Some(as_token);
        self
    }

    pub fn users(mut self, users: Vec<String>) -> PublisherBuilder {
        self.users.extend(users);
        self
    }

    pub fn domain(mut self, domain: String) -> PublisherBuilder {
        self.domain = Some(domain);
        self
    }

    pub fn bot_username(mut self, bot_username: String) -> PublisherBuilder {
        self.bot_username = Some(bot_username);
        self
    }

    #[instrument(level = "debug", skip(self))]
    pub async fn build(self) -> Result<Publisher> {
        let storage = self.storage
            .context("Storage not set while building publisher")?;
        let homeserver = self.homeserver
            .context("Homeserver not set while building publisher")?;
        let as_token = self.as_token
            .context("AS token not set while building publisher")?;
        let domain = self.domain
            .context("Domain not set while building publisher")?;
        let bot_username = self.bot_username
            .context("Bot username not set while building publisher")?;

        let client = ruma::client::Client::builder()
                .homeserver_url(homeserver)
                .access_token(Some(as_token))
                .build::<Reqwest>().await?;

        let mut users = Vec::new();
        for u in &self.users {
            let id = UserId::parse(format!("@{u}:{domain}"))?;
            users.push(id);
        }

        let bot_fullname = UserId::parse(format!("@{bot_username}:{domain}"))?;

        Ok(Publisher {
            storage,
            client,
            users,
            bot_fullname
        })
    }
}
