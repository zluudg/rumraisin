use std::collections::HashMap;

use anyhow::{bail, Context, Result};
use lazy_static::lazy_static;
use roxmltree::{Document, Node};
use tracing::instrument;

use crate::storage::FeedModel;
use crate::storage::EntryModel;

const OUTERMOST_TAG_RSS: &str = "rss";
const OUTERMOST_TAG_ATOM: &str = "feed";
const FEED_KIND_RSS: &str = OUTERMOST_TAG_RSS;
const FEED_KIND_ATOM: &str = "atom";

lazy_static! {
    static ref SANITIZE_REPLACEMENTS: HashMap<String, String> = HashMap::from([
        ("&nbsp;".to_string(), "&#32;".to_string()),
    ]);
}

#[instrument(skip(body), level = "debug")]
pub fn parse_into_feed(url: &String, body: &String) -> Result<FeedModel> {
    let sanitized_body = sanitize_body(body);
    let doc = Document::parse(&sanitized_body)?;
    let doc = doc.root_element();

    let kind = get_feed_kind(&doc)?;

    let doc = doc.children().find(
        |n| n.tag_name().name() == "channel"
    ).unwrap_or(doc);

    let name = doc.children().find(
        |n| n.tag_name().name() == "title"
    ).context(format!("Feed missing title tag"))?;
    let name = name.text().unwrap_or(url).to_string();

    Ok(FeedModel {
        name: name.clone(),
        url: url.clone(),
        kind: kind.clone(),
        ..Default::default()
    })
}

#[instrument(skip(body), level = "debug")]
pub fn parse_into_entries(url: &String, body: &String)
    -> Result<Vec<EntryModel>> {
    let sanitized_body = sanitize_body(body);
    let doc = Document::parse(&sanitized_body)?;
    let doc = doc.root_element();
    let kind = get_feed_kind(&doc)?;
    let content_tag: String;

    let doc = match kind.as_str() {
        FEED_KIND_RSS => {
            content_tag = String::from("item");
            doc.children().find(|n| n.tag_name().name() == "channel")
                .context("Feed missing channel tag")?
        },
        FEED_KIND_ATOM => {
            content_tag = String::from("entry");
            doc
        },
        _ => bail!("Bad feed kind!"),
    };

    let mut entries = vec![];
    for raw in doc.children().filter(|c| c.tag_name().name() == content_tag) {
        let entry = match kind.as_str() {
            FEED_KIND_RSS => create_rss_entry(raw, url)?,
            FEED_KIND_ATOM => create_atom_entry(raw, url)?,
            _ => bail!("Bad feed kind!"),
        };
        entries.push(entry);
    }

    Ok(entries)
}

fn get_feed_kind(root: &Node) -> Result<String> {
    let kind = match root.tag_name().name() {
        OUTERMOST_TAG_RSS => FEED_KIND_RSS.to_string(),
        OUTERMOST_TAG_ATOM => FEED_KIND_ATOM.to_string(),
        _ => bail!("Unexpected outermost tag, cannot determine feed kind"),
    };

    Ok(kind)
}

fn create_rss_entry(entry: Node, url: &String) -> Result<EntryModel> {
    let link = entry.children().find(|n| n.tag_name().name() == "link")
        .context("RSS entry missing link tag")?
        .text()
        .context("RSS entry has bad link tag contents")?;

    let title = entry.children().find(|n| n.tag_name().name() == "title")
        .context("RSS entry missing title tag")?
        .text()
        .context("RSS entry has bad title tag contents")?;

    let guid = entry.children().find(|n| n.tag_name().name() == "guid")
        .context("RSS entry missing guid tag")?
        .text()
        .context("RSS entry has bad guid tag contents")?;

    let tmp = EntryModel {
        title: title.to_string(),
        link: link.to_string(),
        source: url.clone(),
        kind: FEED_KIND_RSS.to_string(),
        is_published: false,
        uid: guid.to_string(),
        hash: 0,
    };

    Ok( EntryModel {
        hash: tmp.get_hash(),
        ..tmp
    })
}

fn create_atom_entry(entry: Node, url: &String) -> Result<EntryModel> {
    let link = entry.children().find(|n| n.tag_name().name() == "link")
        .context("Atom entry missing link tag")?
        .attribute("href")
        .context("Atom entry missing href attribute")?;

    let title = entry.children().find(|n| n.tag_name().name() == "title")
        .context("Atom entry missing title tag")?
        .text()
        .context("Atom entry has bad title tag contents")?;

    let id = entry.children().find(|n| n.tag_name().name() == "id")
        .context("Atom entry missing id tag")?
        .text()
        .context("Atom entry has bad guid tag contents")?;

    let tmp = EntryModel {
        title: title.to_string(),
        link: link.to_string(),
        source: url.clone(),
        kind: FEED_KIND_ATOM.to_string(),
        is_published: false,
        uid: id.to_string(),
        hash: 0,
    };

    Ok( EntryModel {
        hash: tmp.get_hash(),
        ..tmp
    })
}

fn sanitize_body(body: &String) -> String {
    let mut body_new = body.clone();
    for (bad, good) in SANITIZE_REPLACEMENTS.iter() {
        body_new = body_new.replace(bad, good);
    }

    body_new
}

#[cfg(test)]
mod test { // TODO parameterize somehow to avoid repetition?
    use std::fs;
    use super::*;

    fn get_test_feed(file_path: &str) -> String{
        fs::read_to_string(file_path.to_string())
            .expect("Could not read test input")
    }

    #[test]
    fn parabol_press_1_parse_feed() {
        let feed = get_test_feed("test_feeds/parabol-press-2024-04-22.feed");
        assert!(parse_into_feed(&"dummy".to_string(), &feed).is_ok())
    }

    #[test]
    fn daring_fireball_1_parse_feed() {
        let feed =
            get_test_feed("test_feeds/daringfireball-net-2024-04-22.feed");
        assert!(parse_into_feed(&"dummy".to_string(), &feed).is_ok())
    }

    #[test]
    fn ntia_gov_1_parse_feed() {
        let feed = get_test_feed("test_feeds/ntia-gov-2024-04-22.feed");
        assert!(parse_into_feed(&"dummy".to_string(), &feed).is_ok())
    }

    #[test]
    fn parabol_press_1_parse_entries() {
        let entries = get_test_feed("test_feeds/parabol-press-2024-04-22.feed");
        assert!(parse_into_entries(&"dummy".to_string(), &entries).is_ok())
    }

    #[test]
    fn daring_fireball_1_parse_entries() {
        let entries =
            get_test_feed("test_feeds/daringfireball-net-2024-04-22.feed");
        assert!(parse_into_entries(&"dummy".to_string(), &entries).is_ok())
    }

    #[test]
    fn ntia_gov_1_parse_entries() {
        let entries =
            get_test_feed("test_feeds/ntia-gov-2024-04-22.feed");
        assert!(parse_into_entries(&"dummy".to_string(), &entries).is_ok())
    }
}
