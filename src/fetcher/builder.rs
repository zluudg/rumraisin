use anyhow::{Result, Context};
use tracing::instrument;

use crate::Storage;

use super::Fetcher;
use super::parser;

#[derive(Default, Debug)]
pub struct FetcherBuilder {
    storage: Option<Storage>,
    default_urls: Vec<String>,
}

impl FetcherBuilder {

    pub fn storage(mut self, storage: Storage) -> FetcherBuilder {
        self.storage = Some(storage);
        self
    }

    pub fn default_urls(mut self, urls: Vec<String>) -> FetcherBuilder {
        self.default_urls.extend(urls);
        self
    }

    #[instrument(level = "debug")]
    pub async fn build(self) -> Result<Fetcher> {
        let storage = self.storage.clone()
            .context("Storage not set while building fetcher")?;

        for url in &self.default_urls {
            // TODO continue if feed is already in db to avoid unneeded HTTP GET
            let body = reqwest::get(url).await?.text().await?;
            let feed = parser::parse_into_feed(&url, &body)?;
            let _ = storage.store_feed(feed).await?;
        };

        Ok( Fetcher {
            storage: storage,
            ..Default::default()
        })
    }
}
