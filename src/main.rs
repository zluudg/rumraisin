mod cli;
mod config;
mod fetcher;
mod publisher;
mod storage;
mod subcommands;

use std::error::Error;
use std::path::Path;

use clap::Parser;
use figment::{
    providers::{Format, Env, Yaml, Serialized},
    Figment,
};
use tokio::{
    time::{sleep, Duration},
    signal
};
use tracing::{
    debug,
    info,
    info_span,
    Level,
};
use tracing_subscriber::{
    filter,
    fmt,
    fmt::format::FmtSpan,
    prelude::*,
    registry,
    util::TryInitError,
};

use crate::cli::Cli;
use crate::config::Config;
use crate::storage::Storage;
use crate::fetcher::Fetcher;
use crate::publisher::Publisher;
use crate::subcommands::generate;

static ENV_VARS: &'static [&str] = &[
    "config_file",
    "workdir"
];

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Get configuration from env vars, cli args and file
    let (config, subcommand) = get_config()?;

    // If a subcommands was provided, run that instead of the main loop
    match subcommand {
        Some(cli::Subcommands::Generate(_)) => {
            let _ = generate::generate(&config)?;
            return Ok(())
        }
        None => ()
    }

    // Set tracing configuration
    let _ = set_tracing(&config)?;
    let _span = info_span!("main").entered();
    info!(config.config_file, config.workdir, "Started rumraisin.");
    info!("Running with bridge config: {:?}", config.bridge); // TODO tracing::Value trait?
    debug!(config.debug, "Debug output enabled.");

    // Create database if it doesn't exist
    let storage = Storage::builder()
        .workdir(config.workdir.clone())
        .build().await?;

    // Create the fetcher
    let fetcher = Fetcher::builder()
        .storage(storage.clone())
        .default_urls(config.bridge.feeds) // TODO Move into Fetcher method
        .build().await?;

    // Create the publisher
    let publisher = Publisher::builder()
        .storage(storage.clone())
        .homeserver(config.homeserver.address)
        .as_token(config.appservice.as_token)
        .users(config.bridge.users)
        .domain(config.homeserver.domain)
        .bot_username(config.appservice.bot_username)
        .build().await?;

    // Do some preparatory tasks with the Matrix server
    let _ = publisher.register_appservice_user().await?;
    let _ = publisher.init_feed_rooms().await?;

    // Main fetch loop
    let t1 = tokio::spawn(async move {
        loop {
            fetcher.fetch_entries().await.unwrap();
            sleep(Duration::from_secs(config.bridge.fetch_period)).await;
        }
    });

    // Main publish loop
    let t2 = tokio::spawn(async move {
        sleep(Duration::from_secs(30)).await;
        loop {
            publisher.publish_entries().await.unwrap();
            sleep(Duration::from_secs(config.bridge.publish_period)).await;
        }
    });

    // Listen for CTRL+C
    let terminator = tokio::spawn(signal::ctrl_c());

    // Let the tasks to their thing
    tokio::select! {
        _ = t1 => {},
        _ = t2 => {},
        _ = terminator => {},
    }

    Ok(())
}

fn get_config() -> Result<(Config, Option<cli::Subcommands>), figment::Error> {
    // Parse cli args
    let cli_args = Cli::parse();
    let subcommand = cli_args.subcommand.clone();
    let tmp = Figment::new()
        .merge(Serialized::defaults( Config { ..Default::default() }))
        .merge(Env::prefixed("RUMRAISIN_").only(ENV_VARS))
        .merge(cli_args.clone());

    // Find the config file
    let config_file = tmp.extract_inner::<String>("config_file").unwrap();
    if !Path::new(&config_file).exists() {
        // TODO Error trace and process exit
        return Err(figment::Error::from("Config file not found!".to_string()));
    }

    // Create raw config, DEFAULTS < ENV VARS < CONFIG FILE < CLI ARGS
    let raw_config = Figment::new()
        .merge(Serialized::defaults(Config { ..Default::default() }))
        .merge(Env::prefixed("RUMRAISIN_").only(ENV_VARS))
        .merge(Yaml::file(config_file))
        .merge(cli_args);

    let conf = raw_config.extract::<Config>()?;
    Ok((conf, subcommand))
}

fn set_tracing(config: &Config) -> Result<(), TryInitError> {
    let fmt_layer = fmt::layer()
        .with_target(true)
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .compact();

    let filter = match config.debug {
        0 => {
            filter::Targets::new()
            .with_target("rumraisin", Level::INFO)
            .with_target("ruma_client", Level::WARN)
        },
        1 => {
            filter::Targets::new()
            .with_target("rumraisin", Level::DEBUG)
            .with_target("ruma_client", Level::WARN)
            .with_target("sea_orm", Level::DEBUG)
        },
        _ => {
            filter::Targets::new()
            .with_target("rumraisin", Level::TRACE)
            .with_target("ruma_client", Level::TRACE)
        }
    };

    registry()
        .with(fmt_layer)
        .with(filter)
        .try_init()
}
