use serde::{Serialize, Deserialize};

use std::env::current_dir;
use std::path::PathBuf;

#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    pub config_file: String,
    pub workdir: String,
    pub debug: u8,
    pub homeserver: HomeserverConfig,
    pub appservice: AppserviceConfig,
    pub bridge: BridgeConfig,
    pub subcommands_config: SubcommandsConfig,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            config_file: "config.yaml".to_string(),
            workdir: current_dir()
                     .unwrap_or(PathBuf::from("."))
                     .to_str()
                     .unwrap_or(".")
                     .to_string(),
            debug: 0,
            homeserver: Default::default(),
            appservice: Default::default(),
            bridge: Default::default(),
            subcommands_config: Default::default(),
        }
    }
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct HomeserverConfig {
    pub address: String,
    pub domain: String,
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct AppserviceConfig {
    pub id: String,
    pub bot_username: String,
    pub as_token: String,
    pub hs_token: String,
    pub address: String
}

#[derive(Debug, Deserialize, Serialize)]
pub struct BridgeConfig {
    pub feeds: Vec<String>,
    pub users: Vec<String>,
    pub fetch_period: u64, // In seconds
    pub publish_period: u64, // In seconds
}

impl Default for BridgeConfig {
    fn default() -> Self {
        BridgeConfig {
            feeds: Default::default(),
            users: Default::default(),
            fetch_period: 3600,
            publish_period: 3600,
        }
    }
}

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct SubcommandsConfig {
    pub generate_config: GenerateConfig,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct GenerateConfig {
    pub config_file_out: String,
    pub registration_file_out: String,
    pub address: String,
    pub domain: String,
}

impl Default for GenerateConfig {
    fn default() -> Self {
        GenerateConfig {
            config_file_out: "config.yaml".to_string(),
            registration_file_out: "registration.yaml".to_string(),
            address: "https://example.com".to_string(),
            domain: Default::default(),
        }
    }
}
