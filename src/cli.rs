use clap::{Args, Parser, Subcommand};
use figment::{
    Metadata,
    Profile,
    Provider,
    error::Result,
    value::{Map, Dict, Value},
};
use serde::{Serialize, Deserialize};

#[derive(Parser, Clone, Debug, Serialize, Deserialize)]
#[command(
    author,
    version,
    about,
    long_about = None,
    args_conflicts_with_subcommands = true)]
pub struct Cli {

    #[command(subcommand)]
    pub subcommand: Option<Subcommands>,

    #[arg(short, long, value_parser)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub config_file: Option<String>,

    #[arg(short, long, value_parser)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub workdir: Option<String>,

    #[arg(short, long, value_parser)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fetch_period: Option<u64>,

    #[arg(short, long, value_parser)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub publish_period: Option<u64>,

    #[arg(short, action = clap::ArgAction::Count)]
    pub debug: u8,
}

#[derive(Subcommand, Clone, Debug, Serialize, Deserialize)]
pub enum Subcommands {
    Generate(GenerateArgs),
}

#[derive(Args, Clone, Default, Debug, Serialize, Deserialize)]
pub struct GenerateArgs { // Is "pub" only to avoid compiler warning

    #[arg(short, long, value_parser)]
    #[serde(skip_serializing_if = "Option::is_none")]
    config_file_out: Option<String>,

    #[arg(short, long, value_parser)]
    #[serde(skip_serializing_if = "Option::is_none")]
    registration_file_out: Option<String>,

    #[arg(short, long, value_parser)]
    #[serde(skip_serializing_if = "Option::is_none")]
    address: Option<String>,

    #[arg(short, long, value_parser)]
    #[serde(skip_serializing_if = "Option::is_none")]
    domain: Option<String>,
}

impl Provider for Cli {
    fn metadata(&self) -> figment::Metadata {
        let metadata = Metadata::named("Glorious Cli Provider");
        metadata
    }


    fn data(&self) -> Result<Map<Profile, Dict>> {
        match &self.subcommand {
            Some(c) => {
                match c {
                    Subcommands::Generate(_) => self.provide_data_generate()
                }
            }
            None => self.provide_data_main()
        }
    }
}

impl Cli {

    // TODO Avoid hardcoding like this, maybe some macro voodoo can be used to
    // generate the code by placing attributes on the Cli struct members.
    fn provide_data_main(&self) -> Result<Map<Profile, Dict>> {
        let mut map = Map::new();
        let profile = Profile::Default;
        let mut top_dict = Dict::new();

        match self.config_file.clone() {
            Some(s) => {
                top_dict.insert("config_file".to_string(), Value::from(s));
            }
            None => ()
        }

        match self.workdir.clone() {
            Some(s) => {
                top_dict.insert("workdir".to_string(), Value::from(s));
            }
            None => ()
        }

        top_dict.insert("debug".to_string(), Value::from(self.debug));

        let mut bridge_dict = Dict::new();

        match self.fetch_period {
            Some(n) => {
                bridge_dict.insert("fetch_period".to_string(), Value::from(n));
            }
            None => ()
        }

        match self.publish_period {
            Some(n) => {
                bridge_dict.insert(
                    "publish_period".to_string(), Value::from(n));
            }
            None => ()
        }

        top_dict.insert("bridge".to_string(), Value::from(bridge_dict));

        map.insert(profile, top_dict);

        Ok(map)

    }

    fn provide_data_generate(&self) -> Result<Map<Profile, Dict>> {
        let mut generate_dict = Dict::new();

        let sc = self.subcommand.clone();
        let args = if let Some(Subcommands::Generate(args)) = sc {
            args
        } else {
            GenerateArgs { ..Default::default() }
        };

        match args.config_file_out {
            Some(f) => {
                generate_dict.insert(
                    "config_file_out".to_string(), Value::from(f));
            }
            None => ()
        }

        match args.registration_file_out {
            Some(f) => {
                generate_dict.insert(
                    "registration_file_out".to_string(), Value::from(f));
            }
            None => ()
        }

        match args.address {
            Some(f) => {
                generate_dict.insert(
                    "address".to_string(), Value::from(f));
            }
            None => ()
        }

        match args.domain {
            Some(f) => {
                generate_dict.insert(
                    "domain".to_string(), Value::from(f));
            }
            None => ()
        }

        let mut subcommands_dict = Dict::new();
        subcommands_dict
            .insert("generate_config".to_string(), Value::from(generate_dict));

        let mut top_dict = Dict::new();
        top_dict
            .insert(
                "subcommands_config".to_string(),
                Value::from(subcommands_dict));

        let mut map = Map::new();
        let profile = Profile::Default;

        map.insert(profile, top_dict);

        Ok(map)
    }
}
