use std::fs;
use std::path::PathBuf;

use anyhow::{Context, Result};
use minijinja::render;
use url::Url;

use crate::Config;

const CONFIG_TEMPLATE: &str = "\
homeserver:
    address: {{ address }}
    domain: {{ domain }}
appservice:
    id: rumraisin
    bot_username: rumraisinbot
    as_token: {{ as_token }}
    hs_token: {{ hs_token }}
    address: http://localhost:30001
bridge:
    feeds:
        - https://www.theregister.com/security/headlines.atom
        - https://techcrunch.com/feed/
    users:
        - someuser
    fetch_period: 3600
    publish_period: 3600\
";

const REGISTRATION_TEMPLATE: &str = "\
id: rumraisin
url: http://localhost:30001
rate_limited: false
as_token: {{ as_token }}
hs_token: {{ hs_token }}
sender_localpart: {{ sender_localpart }}
namespaces:
  users:
    - exclusive: true
      regex: '@rumraisinbot:{{ domain }}'
    - exclusive: true
      regex: '@rumraisin_.*:{{ domain }}'\
";

pub fn generate(config: &Config) -> Result<()>{
    let cf = &config.subcommands_config.generate_config.config_file_out;
    let rf = &config.subcommands_config.generate_config.registration_file_out;
    let mut buf = [0u8; 48];
    getrandom::getrandom(&mut buf)?;
    let as_token = base64_url::encode(&buf);
    getrandom::getrandom(&mut buf)?;
    let hs_token = base64_url::encode(&buf);
    getrandom::getrandom(&mut buf)?;
    let sender_localpart = base64_url::encode(&buf);
    let address = &config.subcommands_config.generate_config.address;
    let mut domain = &config.subcommands_config.generate_config.domain;
    let domain_tmp: String;

    // If no -d flag was provided, attempt to derive from the address value
    if domain.is_empty() {
        domain_tmp = Url::parse(address)?
            .domain()
            .context("Couldn't derive domain for homeserver address")?
            .to_string();
        domain = &domain_tmp;
    }

    let config_contents = render!(CONFIG_TEMPLATE,
        address => address,
        domain => domain,
        as_token => as_token,
        hs_token => hs_token,
    );

    let registration_contents = render!(REGISTRATION_TEMPLATE,
        domain => domain.replace(".", "\\."), // Is regex, must escape periods
        as_token => as_token,
        hs_token => hs_token,
        sender_localpart => sender_localpart,
    );

    let mut cf_path = PathBuf::from(cf);
    if cf_path.is_dir() {
        cf_path.push("config.yaml");
    }
    fs::write(cf_path, config_contents)?;

    let mut rf_path = PathBuf::from(rf);
    if rf_path.is_dir() {
        rf_path.push("registration.yaml");
    }
    fs::write(rf_path, registration_contents)?;

    Ok(())
}
