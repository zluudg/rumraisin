mod builder;

use anyhow::{Result, Context};
use ruma::api::client::{account, room, message};
use ruma::api::error::FromHttpResponseError;
use ruma::api::client::uiaa::UiaaResponse;
use ruma::api::client::error::{ErrorBody, ErrorKind};
use ruma::client::http_client::Reqwest;
use ruma::client::Client;
use ruma::events::AnyMessageLikeEventContent;
use ruma::events::room::message::RoomMessageEventContent;
use ruma::user_id::{UserId, OwnedUserId};
use ruma_client::Error::FromHttpResponse;
use reqwest::header::HeaderValue;
use tracing::{error, info, instrument};

use crate::storage::{EntryModel, FeedModel, Storage};

use self::builder::PublisherBuilder;

#[derive(Debug)]
pub struct Publisher {
    storage: Storage,
    client: Client<Reqwest>,
    users: Vec<OwnedUserId>,
    bot_fullname: OwnedUserId,
}

impl Publisher {

    pub fn builder() -> PublisherBuilder {
        PublisherBuilder::default()
    }

    pub async fn register_appservice_user(&self) -> Result<()> {
        let mut req = account::register::v3::Request::new();
        req.password = None;
        req.username = Some(self.bot_fullname.localpart().to_string());
        req.device_id = None;
        req.initial_device_display_name = None;
        req.auth = None;
        req.kind = account::register::RegistrationKind::User;
        req.inhibit_login = true;
        req.login_type = Some(account::register::LoginType::ApplicationService);
        req.refresh_token = true;

        let _ = self.client.send_customized_request(
                req,
                |r| {
                    let mut v: String = String::from("Bearer ");
                    let token = match self.client.access_token() {
                        Some(t) => t,
                        None => {
                            error!("Missing access token");
                            "".to_string()
                        }
                    };
                    v.push_str(&token);
                    r.headers_mut().insert(
                        "Authorization",
                        HeaderValue::from_str(&v).unwrap());
                    Ok(())
                }
            ).await.map(|_| ()).or_else(|e| {
            if matches!( // Return Ok(()) if error is 'UserInUse'
                &e,
                FromHttpResponse(
                    FromHttpResponseError::Server(
                        UiaaResponse::MatrixError(
                            ruma::api::client::Error { 
                                body: ErrorBody::Standard { 
                                    kind: ErrorKind::UserInUse,
                                    ..
                                },
                                ..
                            }
                        )
                    )
                )) { info!("Appservice user exists"); Ok(()) } else { Err(e) }
            })?;

        Ok(())
    }

    pub async fn init_feed_rooms(&self) -> Result<()> {
        let feeds = self.storage.get_feeds().await?;
        for feed in feeds {
            if feed.room.is_none() {
                info!("Creating matrix room for feed {}", feed.url);
                let mut req = room::create_room::v3::Request::new();
                req.creation_content = None;
                req.initial_state = Vec::new();
                req.invite = self.users.clone();
                req.invite_3pid = Vec::new();
                req.is_direct = false;
                req.name = Some(feed.name.clone());
                req.power_level_content_override = None;
                req.preset = Some(room::create_room::v3::RoomPreset::PrivateChat);
                req.room_alias_name = None;
                req.room_version = None;
                req.topic = None;
                req.visibility = room::Visibility::Private;

                let username = UserId::parse(self.bot_fullname.clone())?;
                let res = self.client.send_request_as(&username, req).await?;

                let updated = FeedModel {
                    room: Some(res.room_id.as_str().to_string()),
                    ..feed
                };
                self.storage.store_feed(updated).await?;
            }
        }

        Ok(())
    }

    #[instrument(skip(self), level = "info")]
    pub async fn publish_entries(&self) -> Result<()> {
        let mut entries = self.storage.get_unpublished_entries().await?;
        for e in &mut entries {
            let _ = self.publish_entry(&e).await?;
            e.is_published = true;
        }
        let _ = self.storage.store_entries(entries, true).await?;

        Ok(())
    }

    #[instrument(skip(self))]
    async fn publish_entry(&self, entry: &EntryModel) -> Result<()> {
        let text = format!(
            "Title\n    {}\nLink\n    {}",
            entry.title,
            entry.link
        );
        let content = RoomMessageEventContent::text_plain(text);
        let content = AnyMessageLikeEventContent::RoomMessage(content);

        let feed = self.storage.get_feed_by_entry(entry).await?
            .context(format!(
                        "No feed found that contains entry {}",
                        entry.link
                    )
            )?;
        let room_id = feed.room
            .context(format!("Feed {} has no associated room!", feed.url))?;
        let room_id = ruma::RoomId::parse(room_id)?;
        let txn_id = entry.get_hash().to_string().into();
        let req = message::send_message_event::v3::Request::new(
            room_id,
            txn_id,
            &content
        )?;

        let username = UserId::parse(self.bot_fullname.clone())?;
        let _ = self.client.send_request_as(&username, req).await?;

        Ok(())
    }
}
