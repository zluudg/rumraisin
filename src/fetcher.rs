mod builder;
mod parser;

use anyhow::Result;
use tracing::{debug, error, instrument};

use crate::Storage;

use self::builder::FetcherBuilder;

#[derive(Debug)]
pub struct Fetcher {
    storage: Storage,
    fetch_attempts: u64,
}

impl Default for Fetcher {

    fn default() -> Self {
        Self {
            storage: Default::default(),
            fetch_attempts: 5,
        }
    }
}

impl Fetcher {

    pub fn builder() -> FetcherBuilder {
        FetcherBuilder::default()
    }

    #[instrument(skip(self), level = "info")]
    pub async fn fetch_entries(&self) -> Result<()> {
        let feeds = self.storage.get_feeds().await?;
        for feed in feeds {
            let mut count = 0;

            let body = loop {
                let rsp = reqwest::get(&feed.url).await;

                if count < self.fetch_attempts {
                    // Break if we got something, else try again
                    if let Ok(r) = rsp {
                        break Ok(r)
                    } else {
                        debug!("Fetching feed failed! Trying again...");
                        count = count + 1;
                    }
                } else {
                    // Done trying, return whatever we have, even if it's an err
                    break rsp
                }
            };

            let body = body?;
            let status_code = body.status();
            let body = body.text().await?;

            let entries_res = parser::parse_into_entries(&feed.url, &body);
            let entries = if let Ok(ents) = entries_res {
                ents
            } else {
                error!("Failed parsing entries! Status Code= {}, body={}",
                       status_code,
                       body);
                vec![]
            };
            let _ = self.storage.store_entries(entries, false).await?;
        }

        Ok(())
    }
}
